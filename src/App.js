import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import ListaDispositivos from './components/ListaDispositivos';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path='/' element={ <ListaDispositivos/> } />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
