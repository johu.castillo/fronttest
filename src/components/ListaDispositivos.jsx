import React, { useEffect, useState } from "react";
import axios from "axios";

const base_url = 'http://127.0.0.1:8000/api'

const ListaBodegas = () => {
   const [bodegas, setBodegas] = useState([])
   const [marcas, setMarcas] = useState([])
   const [modelos, setModelos] = useState([])

   const [selectedBodega, setSelectedBodega] = useState('')
   const [selectedMarca, setSelectedMarca] = useState('')
   const [selectedModelo, setSelectedModelo] = useState('')

   const [dispositivos, setDispositivos] = useState([])
   
   const onChangeBodega = function (e) {
      setSelectedBodega(e.target.value);
   }

   const onChangeMarca = function (e) {
      setSelectedMarca(e.target.value);
   }

   const onChangeModelo = function (e) {
      setSelectedModelo(e.target.value);
   }

   useEffect ( () => {
      getAllBodegas()
      getAllMarcas()
      getAllModelos()
   }, [])

   useEffect ( () => {
      if (selectedMarca){
         getAllModelos(selectedMarca)
         setSelectedModelo('')
      }
   }, [selectedMarca])

   const getAllBodegas = () => {
      axios.get(`${base_url}/bodegas/get-bodegas`).then((res) => {
         setBodegas(res.data.content)
      })
   }

   const getAllMarcas = () => {
      axios.get(`${base_url}/marcas/get-marcas`).then((res) => {
         setMarcas(res.data.content)
      })
   }

   const getAllModelos = (marca_id) => {
      axios.get(`${base_url}/modelos/get-modelos/${marca_id}`).then((res) => {
         setModelos(res.data.content)
      })
   }

   const getDispositivos = () => {
      console.log('selectedBodega: ', selectedBodega);
      console.log('selectedMarca: ', selectedMarca);
      console.log('selectedModelo: ', selectedModelo);
      const obj = {
         bodegaId: selectedBodega,
         marcaId: selectedMarca,
         modeloId: selectedModelo,
      }
      axios.post(`${base_url}/dispositivos/get`, obj).then((res) => {
         console.log(res);
         setDispositivos(res.data.content)
      })
   }

   return (
      <div className="container">
         <div className="form-group row d-flex p-2 justify-content-center align-items-center">
            <div className="col-3">
               <label>
                  <p className="m-2"> Bodegas </p>
                  <select className="form-control" name="bodegas" defaultValue={'default'} onChange={onChangeBodega}>
                     <option value="default" disabled> Seleccione... </option>

                     { bodegas.map((bodega) => (
                        <option key={bodega.id} value={bodega.id}> {bodega.nombre} </option>
                     ))}
                  </select>
               </label>
            </div>

            <div className="col-3">
               <label>
               <p className="m-2"> Marcas </p>
                  <select className="form-control" name="marcas" defaultValue={'default'} onChange={onChangeMarca}>
                     <option value="default" disabled> Seleccione... </option>

                     { marcas.map((marca) => (
                        <option key={marca.id} value={marca.id}> {marca.nombre} </option>
                     ))}
                  </select>
               </label>
            </div>

            <div className="col-3">
               <label>
               <p className="m-2"> Modelos </p>
                  <select className="form-control" name="modelos" defaultValue={'default'} onChange={onChangeModelo}>
                     <option value="default" disabled> Seleccione... </option>

                     { modelos.map((modelo) => (
                        <option key={modelo.id} value={modelo.id}> {modelo.nombre} </option>
                     ))}
                  </select>
               </label>
            </div>

            <div className="col-3">
               <button className="mt-3 m-1 p-3 btn btn-primary btn-search" onClick={getDispositivos}> Buscar </button>
            </div>
         </div>

         <div className="mt-5">
            <table className="table table-striped">
               <thead className="bg-primary text-white">
                  <tr>
                     <th>ID</th>
                     <th>Nombre</th>
                     <th>Marca</th>
                     <th>Modelo</th>
                     <th>Bodega</th>
                  </tr>
               </thead>
               <tbody>
                  { dispositivos.length ? dispositivos.map((dispositivo) => (
                     <tr key={dispositivo.id }>
                        <td> { dispositivo.id } </td>
                        <td> { dispositivo.nombre } </td>
                        <td> { dispositivo.modelo.nombre } </td>
                        <td> { dispositivo.modelo.marca.nombre } </td>
                        <td> 
                           { dispositivo.bodegas.map((bodega) => (
                              <>
                                 <span>{ bodega.nombre }, </span>
                                 <br/>
                              </>
                           )) 
                           } 
                        </td>
                     </tr>
                  )) 
                  : 
                     <tr>
                        <td> { 'No hay dispositivos con este filtro.' } </td>
                     </tr>
                  }
               </tbody>
            </table>
         </div>
         
      </div>
   )
}

export default ListaBodegas